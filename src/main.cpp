/**
 * @file main.cpp
 * @author Malo Massieu Rocabois
 * @brief Main file for my sorting algorithm visualizer
 * @version 0.1
 * @date 2022-11-30
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "../includes/main.hpp"

size_t it1 = 0;
size_t it2 = 0;
size_t it3 = -1;
bool finished = false;
int thickness = 6;
int gap = 2;
float speed = 0;
int choice;
bool restart = true;

sf::Font font;


/**
 * @brief Program entry point
 * 
 * @return int 0 if everything went well
 */
int main(void)
{

    srand((unsigned) time(NULL));

    do
    {
        
        std::cout << "1. Bubble Sort\n2. Simple Sort\n3. Quick Sort\n> ";

        std::cin >> choice;

    } while (choice < 1 || choice > 3);

    sf::VideoMode size(1800, 1000);

    sf::RenderWindow window(size, "SFML App", sf::Style::Titlebar | sf::Style::Close);

    if (!font.loadFromFile("font/Roboto.ttf")) {
        
        std::cout << "Couldn't load font." << std::endl;

        return EXIT_FAILURE;

    }

    std::vector<int> tab;

    while (window.isOpen())
    {

        if (restart) {

            finished = false;

            tab = generateTab(window);

            usleep(1000000);

            shuffleTab(window, tab);

            usleep(1000000);

            restart = false;

        }

        if (!finished) {

            if (choice == 1) {

                bubbleSort(window, tab);

            } else if (choice == 2) {

                simpleSort(window, tab);

            } else {

                quickSort(window, tab, 0, tab.size() - 1);

            }

        }

        display(window, tab);

        handleInputs(window);

    }

    return EXIT_SUCCESS;

}

/**
 * @brief Handles all the necessary inputs
 * 
 * @param window The main window
 */
void handleInputs(sf::RenderWindow &window)
{

    sf::Event event;

    while (window.pollEvent(event))
    {

        if (event.type == sf::Event::Closed) {

            finished = true;

            window.close();

        } else if (event.type == sf::Event::KeyPressed) {

            switch ( event.key.code)
            {

                case sf::Keyboard::Escape :

                    finished = true;

                    window.close();

                    break;
                
                case sf::Keyboard::Up :

                    speed = speed + 500;

                    break;

                case sf::Keyboard::Down :

                    if (speed > 0) {

                        speed = speed - 500;

                    }

                    break;

                case sf::Keyboard::Space :

                    restart = true;

                    break;

                default :

                    break;

            }

        }

    }

}

/**
 * @brief Displays everything to the screen
 * 
 * @param window The main window
 * @param tab The main array
 */
void display(sf::RenderWindow &window, std::vector<int> const &tab)
{

    window.clear();

    displayText(window);

    drawLines(window, tab);

    window.display();

}

/**
 * @brief Draws the lines by using the int value in the array to make the line's height
 * 
 * @param window The main window
 * @param tab The tab of value used to determine line's height
 */
void drawLines(sf::RenderWindow &window, std::vector<int> const &tab)
{

    int x = 0;

    for (size_t i = 0; i < tab.size(); i++)
    {

        sf::RectangleShape line(sf::Vector2f(thickness, tab[i]));

        line.setPosition(x, window.getSize().y);

        line.rotate(180);

        if (choice == 1) {

            if (i == it2 + 1) {

                line.setFillColor(sf::Color(255, 0, 0));

            }

        } else {

            if (i == it1 || i == it2) {

                line.setFillColor(sf::Color(255, 0, 0));

            }

        }

        if (choice == 3) {

            if (i > it1 && i < it2) {

                line.setFillColor(sf::Color(0, 0, 255));

            }

        }

        if (i == it3) {

            line.setFillColor(sf::Color(0, 0, 255));

        }
        
        if (finished) {

            line.setFillColor(sf::Color(0, 255, 0));

        }

        window.draw(line);

        x = x + thickness + gap;

    }

}

/**
 * @brief Displays the text containing the delay ant the number of lines
 * 
 * @param window The main window
 */
void displayText(sf::RenderWindow &window)
{

    sf::Text textDelay;

    textDelay.setFont(font);

    float delay = speed / 1000;

    textDelay.setString("Delay : " + std::to_string(delay) + "ms");

    textDelay.setCharacterSize(30);

    textDelay.setFillColor(sf::Color::White);

    textDelay.setPosition(0, 0);

    window.draw(textDelay);

    sf::Text textLines;

    textLines.setFont(font);

    int nbLines = window.getSize().x / (thickness + gap);

    textLines.setString("Number of lines : " + std::to_string(nbLines));

    textLines.setCharacterSize(30);

    textLines.setFillColor(sf::Color::White);

    int x = window.getSize().x / 2;

    textLines.setPosition(x, 0);

    window.draw(textLines);

}

/**
 * @brief Creates an array containing width random ints ranging between 1 and height
 * 
 * @param width The window's width used to determine the array's size
 * @param height The window's height used to determine the max value of the numbers
 * @return std::vector<int> An array containing random values
 */
std::vector<int> generateTab(sf::RenderWindow &window)
{

    int tempSpeed = speed;

    speed = 5000;

    std::vector<int> tab;

    int nbLines = window.getSize().x / (thickness + gap);

    for (int i = 0; i < nbLines; i++)
    {

        int elem = window.getSize().y / nbLines * i + 10;

        tab.push_back(elem);

        usleep(speed);

        display(window, tab);

        handleInputs(window);

    }

    speed = tempSpeed;

    return tab;

}

/**
 * @brief Shuffles an array
 * 
 * @param window The main window
 * @param tab The array to shuffle
 */
void shuffleTab(sf::RenderWindow &window, std::vector<int> &tab)
{

    int tempSpeed = speed;

    speed = 5000;

    for (size_t i = 0; i < tab.size(); i++)
    {

        int randIndex = rand() % tab.size();

        int temp = tab[i];

        tab[i] = tab[randIndex];

        tab[randIndex] = temp;

        usleep(speed);

        display(window, tab);

        handleInputs(window);

    }

    speed = tempSpeed;

}

/**
 * @brief Performs a bubble sort on an array and displays it
 * 
 * @param window The main window
 * @param tab The array to sort
 */
void bubbleSort(sf::RenderWindow &window, std::vector<int> &tab)
{

    for (it1 = 0; it1 < tab.size(); it1++)
    {

        for (it2 = 0; it2 < tab.size() - it1 - 1; it2++)
        {

            if (tab[it2] > tab[it2 + 1]) {

                swap(tab, it2, it2 + 1);

            }

            finished = isSorted(tab);

            usleep(speed);

            display(window, tab);

            handleInputs(window);

            if (finished) {

                return;

            }

        }

    }

}

/**
 * @brief Performs a simple sort on an array and displays it
 * 
 * @param window The main window
 * @param tab The array to sort
 */
void simpleSort(sf::RenderWindow &window, std::vector<int> &tab)
{

    for (it1 = 0; it1 < tab.size(); it1++)
    {

        // int smallestIndex = it1;

        it3 = it1;

        for (it2 = it1; it2 < tab.size(); it2++)
        {

            if (tab[it2] < tab[it3]) {

                it3 = it2;

            }

            finished = isSorted(tab);

            usleep(speed);

            display(window, tab);

            handleInputs(window);

            if (finished) {

                return;

            }

        }

        swap(tab, it3, it1);

    }

}

/**
 * @brief Performs a quick sort on an array and displays it
 * 
 * @param window The main window
 * @param tab The array to sort
 * @param low The lowest index of the array
 * @param high The highest index of the array
 */
void quickSort(sf::RenderWindow &window, std::vector<int> &tab, int const low, int const high)
{

    if (low < high) {
        
        int pi = partition(window, tab, low, high);

        if (pi == -1) {

            return;

        }

        quickSort(window, tab, low, pi - 1);

        quickSort(window, tab, pi + 1, high);

    }

}

/**
 * @brief Partitions an array
 * 
 * @param window The main window
 * @param tab The array to partition
 * @param low The lowest index of the array
 * @param high The highest index of the array
 * @return int The index of the pivot
 */
int partition(sf::RenderWindow &window, std::vector<int> &tab, int const low, int const high) {
    
    int pivot = tab[high];
    
    it1 = (low - 1);

    for (it2 = low; it2 < high; it2++) {
        
        if (tab[it2] <= pivot) {
            
            it1++;
            
            swap(tab, it1, it2);

        }

        finished = isSorted(tab);

        usleep(speed);

        display(window, tab);

        handleInputs(window);

        if (finished) {

            return -1;

        }

    }
    
    swap(tab, it1 + 1, high);
    
    return (it1 + 1);

}

/**
 * @brief Swaps the values at two given indexes in an array
 * 
 * @param tab The array to operate on
 * @param ind1 The index of the first value
 * @param ind2 The index of the second value
 */
void swap(std::vector<int> &tab, int const ind1, int const ind2)
{
    int temp = tab[ind1];

    tab[ind1] = tab[ind2];

    tab[ind2] = temp;


}

/**
 * @brief Checks if an array of int is sorted from lowest to highest
 * 
 * @param tab The tab to check
 * @return true If the array is sorted  
 * @return false If the array isn't sorted
 */
bool isSorted(std::vector<int> const &tab)
{

    bool res = true;

    size_t i = 0;

    while (i < tab.size() && res)
    {

        if (tab[i - 1] > tab[i]) {

            res = false;

        }

        i++;

    }

    return res;

}
