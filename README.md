# Visual Sorting

Displays different [sorting algorithms](https://en.wikipedia.org/wiki/Sorting_algorithm) using SFML in C++.

![Quick Sort](quick-sort.png)

## Algorithms Available

- [Bubble Sort](https://en.wikipedia.org/wiki/Bubble_sort)
- Simple Sort
- [Quick Sort](https://en.wikipedia.org/wiki/Quicksort)

## How to Use

- Escape : Closes the window
- Arrow Up : Increases delay between frames
- Arrow Down : Decreases delay between frames
- Space Bar : Restarts the selected sort

## Requirements

- [g++](https://gcc.gnu.org/)
- [SFML](https://www.sfml-dev.org/index-fr.php)
- [Make](https://www.gnu.org/software/make/) (optional)
