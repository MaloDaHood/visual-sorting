/**
 * @file main.hpp
 * @author Malo Massieu Rocabois
 * @brief Header file for my visual sorting algorithm
 * @version 0.1
 * @date 2022-11-30
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

#include <SFML/Graphics.hpp>
#include <iostream>
#include <cstdlib>
#include <vector>

#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

void handleInputs(sf::RenderWindow &window);
void display(sf::RenderWindow &window, std::vector<int> const &tab);
void drawLines(sf::RenderWindow &window, std::vector<int> const &tab);
void displayText(sf::RenderWindow &window);
std::vector<int> generateTab(sf::RenderWindow &window);
void shuffleTab(sf::RenderWindow &window, std::vector<int> &tab);
void bubbleSort(sf::RenderWindow &window, std::vector<int> &tab);
void simpleSort(sf::RenderWindow &window, std::vector<int> &tab);
void quickSort(sf::RenderWindow &window, std::vector<int> &array, int const low, int const high);
int partition(sf::RenderWindow &window, std::vector<int> &array, int const low, int const high);
void swap(std::vector<int> &tab, int const ind1, int const ind2);
bool isSorted(std::vector<int> const &tab);
